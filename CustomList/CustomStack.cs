﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CustomList
{
    public class CustomStack<T>
    {
        private readonly T[] _stack;
        private readonly int _size;
        private int _topItemIndex = -1;

        public CustomStack(int size = 12)
        {
            _size = size;
            _stack = new T[size];
        }

        public bool IsEmpty()
        {
            return _topItemIndex == -1;
        }
        private bool IsFull => _size == _topItemIndex + 1;

        public void Push(T value)
        {
            if (IsFull)
            {
                throw new Exception("Stack is full");
            }
            _stack[++_topItemIndex] = value;
        }

        public T Pop()
        {
            if (IsEmpty())
            {
                throw new Exception("Stack is empty");
            }

            var removedItem = _stack[_topItemIndex];
            _stack[_topItemIndex--] = (T)(object)null;
            return removedItem;
        }

        public void Sort()
        {
            for (int i = 0; i < _stack.Length; i++)
            {
                for (int j = i + 1; j < _stack.Length; j++)
                {
                    if (Compare(_stack[j], _stack[i]) == 1)
                    {
                        T temp = _stack[j];
                        _stack[j] = _stack[i];
                        _stack[i] = temp;
                    }
                }
            }
        }
        public int Compare(object x, object y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (y == null)
                {
                    return 1;
                }
                else
                {
                    int retval = x.ToString().Length.CompareTo(y.ToString().Length);

                    if (retval != 0)
                    {
                        return retval;
                    }
                    else
                    {
                        return x.ToString().CompareTo(y);
                    }

                }
            }
        }

        public void LeftRotate(int count)
        {
            for (int i = 0; i < count; i++)
                LeftRotatebyOne();
        }

        public void LeftRotatebyOne()
        {
            int i;
            T temp = _stack[0];
            for (i = 0; i < _stack.Length - 1; i++)
                _stack[i] = _stack[i + 1];

            _stack[i] = temp;
        }

        public T[] GetItems()
        {
            return _stack;
        }

        public void Reverse()
        {
            for (int i = 0; i < _stack.Length; i++)
            {
                for (int j = i + 1; j < _stack.Length; j++)
                {
                    T temp = _stack[j];
                    _stack[j] = _stack[i];
                    _stack[i] = temp;
                }
            }
        }

        public T this[int index]
        {
            get
            {
                return _stack[index];
            }
            set
            {
                _stack[index] = value;
            }
        }

        public void Replace(int index, T item)
        {
            _stack[index] = item;
        }

        public void Remove(int index)
        {
            for (int i = index; i < _stack.Length - 1; i++)
            {
                _stack[i] = _stack[i + 1];
            }
            _topItemIndex--;
        }

        public void Fill()
        {
            var random = new Random();
            for (int i = 0; i < _stack.Length; i++)
            {
                _stack[i] = (T)(object)random.Next(1, 99).ToString();
            }
            _topItemIndex = _stack.Length - 1;
        }

        public int Count()
        {
            return _stack.Length;
        }

        public void Distinct()
        {
            T[] distinctArray = new T[_stack.Length];
            int index = 0;
            bool isDuplicate = false;

            for (int i = 0; i < _stack.Length; i++)
            {
                for (int j = i + 1; j < _stack.Length; j++)
                {
                    isDuplicate = false;
                    if ( _stack[i] != null && _stack[j] != null && _stack[i].Equals(_stack[j]))
                    {
                        isDuplicate = true;
                    }
                    if (isDuplicate == false)
                    {
                        distinctArray[index] = _stack[i];
                        index++;
                    }
                    break;
                }
            }

            for(int i = 0; i < distinctArray.Length; i++)
            {
                _stack[i] = distinctArray[i];
            }

            _topItemIndex = distinctArray.Length - 1;
        }

        public void Clear()
        {
            for (int i = 0; i < _stack.Length; i++)
            {
                _stack[i] = (T)(object)null;
            }
            _topItemIndex = -1;
        }

        public bool Contains(T item)
        {
            bool result = false;
            for (int i = 0; i < _stack.Length; i++)
            {
                if ( _stack[i] != null && _stack[i].Equals(item))
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
