﻿using System;
using static System.Console;
using CustomList;
namespace CustomList.Run
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Enter a stack's length");
            int length = int.Parse(ReadLine());
            var stack = new CustomStack<string>(length);


            while (true)
            {
                WriteLine(" 1 - Push an item to stack, \n 2 - Pop an item from stack, \n 3 - Print all items of stack, \n 4 - to sort stack by item length,  \n 5 - Rotate stack, \n 6 - Replace stack, \n 7 - Reverse stack, \n 8 - Remove item by index, \n 9 - Fill stack by random integers, \n 10 - Find count of stack, \n 11 - Distinct stack, \n 12 - Clear stack, \n 13 - Check if an object exists \n");
                string command = ReadLine();

                switch (command)
                {
                    case "1":
                        {
                            WriteLine("Enter an item");
                            var item = ReadLine();
                            stack.Push(item);
                            break;
                        }
                    case "2":
                        {
                            WriteLine("\n Deleted item: " + stack.Pop());
                            break;
                        }
                    case "3":
                        {
                            for(int i = 0; i < stack.GetItems().Length; i++)
                            {
                                WriteLine(stack[i]);
                            }
                            break;
                        }
                    case "4":
                        {
                            stack.Sort();
                            break;
                        }
                    case "5":
                        {
                            WriteLine("Enter a number to left rotate");
                            int count = int.Parse(ReadLine());
                            stack.LeftRotate(count);
                            break;
                        }
                    case "6":
                        {
                            WriteLine("Enter an index and an item to replace");
                            int index = int.Parse(ReadLine());
                            string item = ReadLine();
                            stack.Replace(index, item);
                            break;
                        }
                    case "7":
                        {
                            stack.Reverse();
                            break;
                        }
                    case "8":
                        {
                            WriteLine("Enter an index to remove an item");
                            int index = int.Parse(ReadLine());
                            stack.Remove(index);
                            break;
                        }
                    case "9":
                        {
                            stack.Fill();
                            break;
                        }
                    case "10":
                        {
                            WriteLine(stack.Count());
                            break;
                        }
                    case "11":
                        {
                            stack.Distinct();
                            break;
                        }
                    case "12":
                        {
                            stack.Clear();
                            break;
                        }
                    case "13":
                        {
                            WriteLine("Enter an index and an item to replace");
                            string item = ReadLine();
                            stack.Contains(item);
                            break;
                        }
                    case "14":
                        {
                            WriteLine("Enter for check if stack is empty");

                            if (stack.IsEmpty())
                            {
                                Console.WriteLine("true");
                            }
                            else
                            {
                                Console.WriteLine("false");
                            }
                            break;
                        }
                }
            }

        }
    }
}
